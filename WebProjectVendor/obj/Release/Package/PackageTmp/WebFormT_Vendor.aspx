﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormT_Vendor.aspx.cs" Inherits="WebProjectVendor.WebFormT_Vendor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Vendor</title>
    <style type="text/css">

    </style>
</head>

<body>
    <form id="form1" runat="server" >
        <div id="grid">

            <a href="WebForm_AddDataVendor.aspx" title="Add new Vendor"><input type="button" value="Add new Vendor"/></a>
            
            <asp:ScriptManager ID="ScriptManager" runat="server" />

            <asp:UpdatePanel ID="UpdatePanel_forGridView" runat="server">
                <ContentTemplate>
                        <!-- Use GridView to show the DB-->
                        <asp:GridView ID="GridView" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" 
                            DataKeyNames="PK_Vendor" DataSourceID="SqlDataVendor" ForeColor="#333333" GridLines="None" 
                            OnSelectedIndexChanged="GridView_SelectedIndexChanged">

                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />

                            <Columns>

                                <asp:BoundField DataField="PK_Vendor" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="PK_Vendor" />

                                <asp:TemplateField HeaderText="Name" SortExpression="VendorName">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxName" runat="server" Text='<%# Bind("VendorName") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator 
                                            ID="RequiredFieldValidatorName" 
                                            runat="server" 
                                            ControltoValidate="TextBoxName"
                                            ErrorMessage="Enter Name"
                                            Text="*" ForeColor="Red"
                                            ></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelName" runat="server" Text='<%# Bind("VendorName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Creation Date" SortExpression="CreationDate" >
                                    <EditItemTemplate>
                                        <!--<asp:TextBox ID="TextBoxCreationDate" runat="server" Text='<%# Bind("CreationDate", "{0:D}") %>'></asp:TextBox> -->
                            
                                        <asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="#999999"
                                            CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" 
                                            SelectedDate='<%# Bind("CreationDate") %>' VisibleDate='<%# Eval("CreationDate") %>' Width="200px">
                                            <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                            <NextPrevStyle VerticalAlign="Bottom" />
                                            <OtherMonthDayStyle ForeColor="#808080" />
                                            <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                            <SelectorStyle BackColor="#CCCCCC" />
                                            <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                                            <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                            <WeekendDayStyle BackColor="#FFFFCC" />
                                        </asp:Calendar>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelCreationDate" runat="server" Text='<%# Bind("CreationDate", "{0:D}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="City" SortExpression="City">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxCity" runat="server" Text='<%# Bind("City") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator 
                                            ID="RequiredFieldValidatorCity" 
                                            runat="server" 
                                            ControltoValidate="TextBoxCity"
                                            ErrorMessage="Enter City"
                                            Text="*" ForeColor="Red"
                                            ></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelCity" runat="server" Text='<%# Bind("City") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Adress" SortExpression="Adress">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxAdress" runat="server" Text='<%# Bind("Adress") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator 
                                            ID="RequiredFieldValidatorAdress" 
                                            runat="server" 
                                            ControltoValidate="TextBoxAdress"
                                            ErrorMessage="Enter Adress"
                                            Text="*" ForeColor="Red"
                                            ></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelCity" runat="server" Text='<%# Bind("Adress") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" ButtonType="Button"/>
                            </Columns>

                            <EditRowStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                        </asp:GridView>

                        <asp:ValidationSummary ID="ValidationSummary" ForeColor="Red" runat="server" />

                        <asp:SqlDataSource ID="SqlDataVendor" runat="server" ConnectionString="<%$ ConnectionStrings:VendorDBConnectionString %>" 
                            DeleteCommand="DELETE FROM [T_Adress] WHERE [PK_Adress] = (SELECT [FK_Adress] FROM [T_Vendor] WHERE [PK_Vendor] = @PK_Vendor)" 
                            InsertCommand="INSERT INTO [T_Vendor] ([VendorName], [CreationDate]) VALUES (@VendorName, @CreationDate)" 
                            SelectCommand="SELECT [PK_Vendor], [VendorName], [CreationDate], [City], [Adress] FROM [T_Vendor] 
                                            INNER JOIN T_Adress ON FK_Adress = PK_Adress" 
                            UpdateCommand="UPDATE [T_Vendor] SET [VendorName] = @VendorName, [CreationDate] = @CreationDate WHERE [PK_Vendor] = @PK_Vendor;
                                            UPDATE [T_Adress] SET [City] = @City, [Adress] = @Adress 
                                                WHERE [PK_Adress] =(SELECT [FK_Adress] FROM [T_Vendor] WHERE [PK_Vendor] = @PK_Vendor)">
                            <DeleteParameters>
                                <asp:Parameter Name="PK_Vendor" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="VendorName" Type="String" />
                                <asp:Parameter DbType="Date" Name="CreationDate" />
                                <asp:Parameter Name="City" Type="String" />
                                <asp:Parameter Name="Adress" Type="String" />
                            </InsertParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="VendorName" Type="String" />
                                <asp:Parameter DbType="Date" Name="CreationDate" />
                                <asp:Parameter Name="PK_Vendor" Type="Int32" />
                                <asp:Parameter Name="City" Type="String" />
                                <asp:Parameter Name="Adress" Type="String" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                    </ContentTemplate>

                </asp:UpdatePanel>
            
        </div>
    </form>
</body>
</html>
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm_Employee.aspx.cs" Inherits="WebProjectVendor.WebForm_Employee" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

             <a href="WebForm_AddEmployee.aspx" title="Add new Employee"><input type="button" value="Add new Employee"/></a>

            <asp:ScriptManager ID="ScriptManager" runat="server" />

            <asp:UpdatePanel ID="UpdatePanel_forGridView" runat="server">
                <ContentTemplate>

                        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" 
                            DataKeyNames="PK_Employee" DataSourceID="SqlDataSourceEmployee" ForeColor="#333333" GridLines="None">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />

                            <Columns>
                                <asp:BoundField DataField="PK_Employee" HeaderText="ID" InsertVisible="False" ReadOnly="True" 
                                    SortExpression="PK_Employee" />

                                <asp:BoundField DataField="LName" HeaderText="LastName" SortExpression="LName" />
                                <asp:BoundField DataField="FName" HeaderText="First Name" SortExpression="FName" />
                                <asp:BoundField DataField="MName" HeaderText="Surname" SortExpression="MName" />
                                <asp:BoundField DataField="EmployeeAdress" HeaderText="Adress" SortExpression="EmployeeAdress" />
                                <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
                                <asp:BoundField DataField="Sallary" HeaderText="Sallary" SortExpression="Sallary" />
                                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                            </Columns>

                            <EditRowStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />

                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSourceEmployee" runat="server" ConnectionString="<%$ ConnectionStrings:VendorDBConnectionString %>" 
                            DeleteCommand="DELETE FROM [T_Employee] WHERE [PK_Employee] = @PK_Employee" 
                            InsertCommand="INSERT INTO [T_Employee] ([FName], [MName], [LName], [EmployeeAdress], [Phone], [Sallary]) 
                                VALUES (@FName, @MName, @LName, @EmployeeAdress, @Phone, @Sallary)" 
                            SelectCommand="SELECT [PK_Employee], [FName], [MName], [LName], [EmployeeAdress], [Phone], [Sallary] FROM [T_Employee] 
                                WHERE ([FK_Vendor] = @FK_Vendor)" 
                            UpdateCommand="UPDATE [T_Employee] SET [FName] = @FName, [MName] = @MName, [LName] = @LName, 
                                [EmployeeAdress] = @EmployeeAdress, [Phone] = @Phone, [Sallary] = @Sallary WHERE [PK_Employee] = @PK_Employee">
                            <DeleteParameters>
                                <asp:Parameter Name="PK_Employee" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="FName" Type="String" />
                                <asp:Parameter Name="MName" Type="String" />
                                <asp:Parameter Name="LName" Type="String" />
                                <asp:Parameter Name="EmployeeAdress" Type="String" />
                                <asp:Parameter Name="Phone" Type="String" />
                                <asp:Parameter Name="Sallary" Type="Decimal" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:CookieParameter CookieName="VendorID" DefaultValue="1" Name="FK_Vendor" Type="Int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="FName" Type="String" />
                                <asp:Parameter Name="MName" Type="String" />
                                <asp:Parameter Name="LName" Type="String" />
                                <asp:Parameter Name="EmployeeAdress" Type="String" />
                                <asp:Parameter Name="Phone" Type="String" />
                                <asp:Parameter Name="Sallary" Type="Decimal" />
                                <asp:Parameter Name="PK_Employee" Type="Int32" />
                            </UpdateParameters>
                        </asp:SqlDataSource>

                    </ContentTemplate>
                </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>

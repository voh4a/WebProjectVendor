﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm_AddDataVendor.aspx.cs" Inherits="WebProjectVendor.WebForm_AddDataVendor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="LabelVendorName" runat="server" Text="Введіть назву компанії"></asp:Label> <br />

            <asp:TextBox ID="TextBoxVendorName" runat="server"></asp:TextBox> 
            <asp:RequiredFieldValidator 
                ID="RequiredFieldValidatorVendorName" 
                runat="server" 
                ErrorMessage="Введіть назву"
                Text="*"
                ControlToValidate="TextBoxVendorName"
                ForeColor="Red">
            </asp:RequiredFieldValidator> <br /> <br />


            <asp:Label ID="LabelCreationDate" runat="server" Text="Введіть дату створення"></asp:Label> <br />

            <asp:Calendar ID="CalendarCreationDate" runat="server" BackColor="White" BorderColor="#999999" 
                CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" 
                ForeColor="Black" Height="180px" Width="200px">
                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                <NextPrevStyle VerticalAlign="Bottom" />
                <OtherMonthDayStyle ForeColor="#808080" />
                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                <SelectorStyle BackColor="#CCCCCC" />
                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                <WeekendDayStyle BackColor="#FFFFCC" />
            </asp:Calendar> <br /> <br />


            <asp:Label ID="LabelCity" runat="server" Text="Введіть місто"></asp:Label> <br />

            <asp:TextBox ID="TextBoxCity" runat="server"></asp:TextBox> 
            <asp:RequiredFieldValidator 
                ID="RequiredFieldValidatorCity" 
                runat="server" 
                ErrorMessage="Введіть місто"
                Text="*"
                ControlToValidate="TextBoxCity"
                ForeColor="Red">
            </asp:RequiredFieldValidator> <br /> <br />


            <asp:Label ID="LabelAdress" runat="server" Text="Введіть адрес"></asp:Label> <br />

            <asp:TextBox ID="TextBoxAdress" runat="server"></asp:TextBox> 
            <asp:RequiredFieldValidator 
                ID="RequiredFieldValidatorAdress" 
                runat="server" 
                ErrorMessage="Введіть адрес"
                Text="*"
                ControlToValidate="TextBoxAdress"
                ForeColor="Red">
            </asp:RequiredFieldValidator> <br /> <br />


            <asp:Button ID="ButtonAdd" runat="server" Text="Додати" onclick="ButtonAdd_Click"/>

            <br /> <br />

            <asp:Label ID="LabelEror" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>

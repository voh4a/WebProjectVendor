﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebProjectVendor
{
    public partial class WebForm_AddEmployee : System.Web.UI.Page
    {
        private SqlConnection connectionDB;

        protected void Page_Load(object sender, EventArgs e)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["VendorDBConnectionString"].ConnectionString;
            connectionDB = new SqlConnection(connectionString);
            connectionDB.Open();
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (connectionDB != null && connectionDB.State != System.Data.ConnectionState.Closed)
            {
                connectionDB.Close();
            }
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            int FK_Vendor = 0;

            if (Request.Cookies["VendorID"] != null)
            {
                FK_Vendor = Convert.ToInt32(Request.Cookies["VendorID"].Value);
            }
            else
                return;

            try
            {
                SqlCommand command = new SqlCommand("INSERT INTO [T_Employee] ([FK_Vendor], [FName], [MName], [LName], [EmployeeAdress], [Phone], [Sallary])"
                                + "VALUES(@FK_Vendor, @FName, @MName, @LName, @EmployeeAdress, @Phone, @Sallary)", connectionDB);

                command.Parameters.AddWithValue("FK_Vendor", FK_Vendor);
                command.Parameters.AddWithValue("FName", TextBoxFirstName.Text);
                command.Parameters.AddWithValue("MName", TextBoxSurname.Text);
                command.Parameters.AddWithValue("LName", TextBoxLastName.Text);
                command.Parameters.AddWithValue("EmployeeAdress", TextBoxAdress.Text);
                command.Parameters.AddWithValue("Phone", TextBoxPhone.Text);
                command.Parameters.AddWithValue("Sallary", Convert.ToDecimal(TextBoxSallary.Text));

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LabelEror.Text = "Помилка---- " + ex.Message;
                LabelEror.ForeColor = Color.Red;
            }

            Response.Redirect("WebForm_Employee.aspx");
        }
    }
}

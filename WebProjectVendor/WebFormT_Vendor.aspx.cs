﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebProjectVendor
{
    public partial class WebFormT_Vendor : System.Web.UI.Page
    {

        public struct Vendor
        {
            public string PK_Vendor { get; set; }
            public string VendorName { get; set; }
            public string CreationDate { get; set; }
            public string City { get; set; }
            public string Adress { get; set; }
        }

       // SqlConnection _connectionDB;

        protected void Page_Load(object sender, EventArgs e)
        {
            //string connectionString = ConfigurationManager.ConnectionStrings["VendorDBConnectionString"].ConnectionString;
            //_connectionDB = new SqlConnection(connectionString);
            //_connectionDB.Open();

            //SqlDataReader reader = null;

            //try
            //{
            //    SqlCommand command = new SqlCommand("SELECT PK_Vendor, VendorName, CreationDate, City, Adress FROM T_Vendor " +
            //        "INNER JOIN T_Adress ON FK_Adress = PK_Adress", _connectionDB);

            //    reader = command.ExecuteReader();
            //    List<Vendor> data = new List<Vendor>();
            //    Vendor temp = new Vendor();

            //    while (reader.Read())
            //    {
            //        temp.PK_Vendor = Convert.ToString(reader["PK_Vendor"]);
            //        temp.VendorName = Convert.ToString(reader["VendorName"]);
            //        temp.CreationDate = Convert.ToString(reader["CreationDate"]);
            //        temp.City = Convert.ToString(reader["City"]);
            //        temp.Adress = Convert.ToString(reader["Adress"]);

            //        data.Add(temp);
            //    }

            //    Repeater1.DataSource = data;
            //    Repeater1.DataBind();

            //}
            //catch(Exception ex)
            //{

            //}
            //finally
            //{
            //    if(reader != null)
            //    {
            //        reader.Close();
            //    }
            //}
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            //if(_connectionDB != null && _connectionDB.State != System.Data.ConnectionState.Closed)
            //{
            //    _connectionDB.Close();
            //}
        }

        protected void GridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = (int)GridView.SelectedDataKey.Values["PK_Vendor"];
            Response.Cookies["VendorID"].Value = Convert.ToString(id);
            Response.Cookies["VendorID"].Expires = DateTime.Now.AddDays(1);

            Response.Redirect("WebForm_Employee.aspx");
        }
    }
}
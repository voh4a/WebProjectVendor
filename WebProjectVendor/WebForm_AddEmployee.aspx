﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm_AddEmployee.aspx.cs" Inherits="WebProjectVendor.WebForm_AddEmployee" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="LabelLastName" runat="server" Text="Фаміля"></asp:Label>  <br />

            <asp:TextBox ID="TextBoxLastName" runat="server"></asp:TextBox>

            <asp:RequiredFieldValidator 
                ID="RequiredFieldValidatorLastName" 
                runat="server" 
                ErrorMessage="Введіть фамілію"
                Text="*"
                ControlToValidate="TextBoxLastName"
                ForeColor="Red">
            </asp:RequiredFieldValidator> <br /> <br />


            <asp:Label ID="LabelFirstName" runat="server" Text="Ім'я"></asp:Label>  <br />

            <asp:TextBox ID="TextBoxFirstName" runat="server"></asp:TextBox>  
            
            <asp:RequiredFieldValidator 
                ID="RequiredFieldValidatorFirstName" 
                runat="server" 
                ErrorMessage="Введіть ім'я"
                Text="*"
                ControlToValidate="TextBoxFirstName"
                ForeColor="Red">
            </asp:RequiredFieldValidator> <br /> <br />

            <asp:Label ID="LabelSurname" runat="server" Text="По батькові"></asp:Label>  <br />

            <asp:TextBox ID="TextBoxSurname" runat="server"></asp:TextBox>  
            
            <asp:RequiredFieldValidator 
                ID="RequiredFieldValidatorSurname" 
                runat="server" 
                ErrorMessage="Введіть по батькові"
                Text="*"
                ControlToValidate="TextBoxSurname"
                ForeColor="Red">
            </asp:RequiredFieldValidator> <br /> <br />

            <asp:Label ID="LabelAdress" runat="server" Text="Введіть адрес"></asp:Label>  <br />

            <asp:TextBox ID="TextBoxAdress" runat="server"></asp:TextBox>  
            
            <asp:RequiredFieldValidator 
                ID="RequiredFieldValidatorAdress" 
                runat="server" 
                ErrorMessage="Введіть адресу"
                Text="*"
                ControlToValidate="TextBoxAdress"
                ForeColor="Red">
            </asp:RequiredFieldValidator> <br /> <br />

            <asp:Label ID="LabelPhone" runat="server" Text="Введіть номер телефону"></asp:Label>  <br />

            <asp:TextBox ID="TextBoxPhone" runat="server"></asp:TextBox>  
            
            <asp:RequiredFieldValidator 
                ID="RequiredFieldValidatorPhone" 
                runat="server" 
                ErrorMessage="Введіть телефон"
                Text="*"
                ControlToValidate="TextBoxPhone" 
                ForeColor="Red">
            </asp:RequiredFieldValidator> 

            <asp:RegularExpressionValidator 
                ID="RegularExpressionValidatorPhone" 
                runat="server" 
                ErrorMessage="Формат має бути наступний (097)7005853"
                ControlToValidate="TextBoxPhone"
                ForeColor="Red"
                ValidationExpression="\(\d{3}\)\d{7}">
            </asp:RegularExpressionValidator> <br /> <br />

            <asp:Label ID="LabelSallary" runat="server" Text="Введіть зарплату"></asp:Label>  <br />

            <asp:TextBox ID="TextBoxSallary" runat="server"></asp:TextBox>  
            
            <asp:RequiredFieldValidator 
                ID="RequiredFieldValidatorSallary" 
                runat="server" 
                ErrorMessage="Введіть зарплату"
                Text="*"
                ControlToValidate="TextBoxSallary"
                ForeColor="Red">
            </asp:RequiredFieldValidator> 
            <asp:RangeValidator 
                ID="RangeValidatorSallary" 
                runat="server" 
                ErrorMessage="Дані повинні бути числові"
                ControlToValidate="TextBoxSallary"
                MinimumValue="1"
                MaximumValue="1000000"
                Type="Double"
                ForeColor="Red">
            </asp:RangeValidator> <br /> <br />

            <asp:Button ID="ButtonSubmit" runat="server" Text="Додати" onclick="ButtonSubmit_Click" /> <br /> <br />

             <asp:Label ID="LabelEror" runat="server"></asp:Label>

        </div>
    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebProjectVendor
{
    public partial class WebForm_AddDataVendor : System.Web.UI.Page
    {
        private SqlConnection connectionDB;

        protected void Page_Load(object sender, EventArgs e)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["VendorDBConnectionString"].ConnectionString;
            connectionDB = new SqlConnection(connectionString);
            connectionDB.Open();
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if(connectionDB != null && connectionDB.State != System.Data.ConnectionState.Closed)
            {
                connectionDB.Close();
            }
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            if(!Page.IsValid)
            {
                return;
            }

            try
            {
                SqlCommand command = new SqlCommand("INSERT INTO T_Adress (City, Adress) VALUES(@City, @Adress) " +
                    " INSERT INTO T_Vendor (FK_Adress, VendorName, CreationDate) SELECT PK_Adress, @VendorName, @CreationDate from T_Adress " +
                    "WHERE City = @City AND Adress = @Adress ", connectionDB);

                command.Parameters.AddWithValue("City", TextBoxCity.Text);
                command.Parameters.AddWithValue("Adress", TextBoxAdress.Text);
                command.Parameters.AddWithValue("VendorName", TextBoxVendorName.Text);
                command.Parameters.AddWithValue("CreationDate", CalendarCreationDate.SelectedDate);

                command.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                LabelEror.Text = "Помилка---- " + ex.Message;
                LabelEror.ForeColor = Color.Red;
            }

            Response.Redirect("WebFormT_Vendor.aspx");
        }
    }
}